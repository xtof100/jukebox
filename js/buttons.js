//---------------------------------------------------
//all functions that control the controller buttons
//---------------------------------------------------


firstEntry = readFirstEntry();
if(firstEntry === null)
{
    writeFirstEntry(1);
}
//button click handeling

    for (let i=0 ; i < 6 ; i++)
    {
        resetDigits();
        btnChar[i].addEventListener("click",setDigit1);
    } 


function setDigit1()
{
    if(btnCount === 0)
    {
        console.log(this.innerText + "clicked")
        //reset display
        resetDigits();

        //set display with choosen character
        digit1.innerText = this.innerText;
        
        //store choosen characters
        adress = this.innerText;

        //activate second digit
        btnCount = 1;
        console.log(btnCount);

        for (let i=0 ; i < 6 ; i++)
        {
            btnChar[i].removeEventListener("click",setDigit1);
        }
        for (let i=0 ; i < 10 ; i++)
        {
            
            btn[i].addEventListener("click",setDigit2);
        } 
    }
}

function setDigit2()
{
    if(btnCount === 1)
    {
        console.log(this.innerText + "clicked")
        digit2.innerText = this.innerText;

        adress += this.innerText;

        btnCount = 2;
        console.log(btnCount);

        for (let i=0 ; i < 10 ; i++)
        {
            btn[i].removeEventListener("click",setDigit2);
            btn[i].addEventListener("click",setDigit3);
            
        } 
    }
}

function setDigit3()
{
    if(btnCount === 2)
    {   
        console.log(this.innerText + "clicked")
        digit3.innerText = this.innerText;
        
        //store adress in playlistNrs and limit to 100 songs in playlist
        adress += this.innerText;
        if(playlistCounter<100)
        {
            playlistNrs.push(adress);    
            playlistCounter ++;  
        }
        

        btnCount = 0;
        console.log(btnCount);
        for (let i=0 ; i < 10 ; i++)
        {
            btn[i].removeEventListener("click",setDigit3);
        } 

        for (let i=0 ; i < 6 ; i++)
        {
            btnChar[i].addEventListener("click",setDigit1);
        } 
    }
    
    WriteToPlayListNrsStorage(playlistNrs);
    
    firstEntry = readFirstEntry();
    if(parseInt(firstEntry) === 2)
    {  
        writeFirstEntry(2);
        addToPlayList();
        
    }

    else if(parseInt(firstEntry) === 1)
    {  
        writeFirstEntry(2);
        loadFirstPlayList();
        playAudio();  
    }
   
}


function resetDigits()
{
    digit1.innerText = "_";
    digit2.innerText = "_";
    digit3.innerText = "_";
    adress = "";
}

btnReset.addEventListener("click",resetPlay);

function resetPlay()
{
    localStorage.clear;
    localStorage.removeItem('firstEntry');  
    localStorage.removeItem('playListNrs');
    location.reload();
}

//page buttons

btnFor.addEventListener("click",forBtn)
btnRew.addEventListener("click",rewBtn)

function forBtn()
{
    flipLabels();
    flipCount= flipCount+360;
    

    if(countForRew === 0)
    {
        countForRew = countForRew + 39;
    } 
    else
    {
        countForRew = countForRew + 40;
    }
    if (countForRew === 159)
    {
        countForRew = 0;
    }
   
   
    setLabels(countForRew);
    
    console.log(countForRew);
}

function rewBtn()
{
    flipLabels();
    flipCount= flipCount-360;


    if (countForRew === 0)
    {
        countForRew = 159;
    }
    
    
    if( countForRew > 40)
    {
        countForRew = countForRew - 40;
    }

    else
    {
        countForRew = 0;
    }

    
    
    setLabels(countForRew);
    
    console.log(countForRew);
    
}



function flipLabels()
{ 
    for(let i=0; i< 20; i++)
    {
        card[i].classList.toggle("flipCard"); 
        //update class
        card[i].style.transform = "rotateX("+flipCount+"deg)";
    }

}



//https://stackoverflow.com/questions/1720320/how-to-dynamically-create-css-class-in-javascript-and-apply
function createClass(name,rules)
{
    var style = document.createElement('style');
    style.type = 'text/css';
    document.getElementsByTagName('head')[0].appendChild(style);
    if(!(style.sheet||{}).insertRule) 
        (style.styleSheet || style.sheet).addRule(name, rules);
    else
        style.sheet.insertRule(name+"{"+rules+"}",0);
}

createClass('.flipCard',"transform: rotateX("+flipCount+"deg);");




