//---------------------------------------------------
//all functions that connect to the local storage
//---------------------------------------------------

//reads library and add a ne song object to it
function WriteToLibrary(libraryArray)
{
    //localStorage.clear;
    localStorage.setItem('lib',JSON.stringify(libraryArray));  
}
//read library and returns the object
function readLibrary()
{
    let dataTemp = JSON.parse(localStorage.getItem('lib'));
    return dataTemp;
}

//read labels and returns the object
function readLabelsStorage()
{
    let dataTemp = JSON.parse(localStorage.getItem('labelStorage'));
    return dataTemp;
}

//write labels to local storage
function WriteToLabelStorage(labelArray)
{
    //localStorage.clear;
    localStorage.setItem('labelStorage',JSON.stringify(labelArray));  
}

//write playlistNrs to local storage
function WriteToPlayListNrsStorage(libraryArray)
{
    //localStorage.clear;
    localStorage.setItem('playListNrs',JSON.stringify(playlistNrs));  
}
//read library and returns the object
function readPlayListNrsFromStorage()
{
    let dataTemp = JSON.parse(localStorage.getItem('playListNrs'));
    return dataTemp;
}

function writeFirstEntry(status) //marker for first entry detection write function
{
    //localStorage.clear;
    localStorage.setItem('firstEntry',status);  
}

function readFirstEntry() //marker for first entry detection write function
{
    let dataTemp = localStorage.getItem('firstEntry');
    return dataTemp;
}

function WriteHide(index)
{
    //localStorage.clear;
    localStorage.setItem('hide',index);  
}
//read library and returns the object
function readHide()
{
    let dataTemp = localStorage.getItem('hide');
    return dataTemp;
}
