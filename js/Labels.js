//set Music to the write labels
//----------------------------------------

//check if Library exists
if (readLibrary())
{
    library = readLibrary();
}


//----------------------------------------------------------------
//all functions that control pages and labels
//----------------------------------------------------------------
//listen to admin page buttons

btnPage1.addEventListener("click",function()
{
    pageAdminSelector = 1; 
    pageAdSelector();
    adminLabelSelectBoxes(0);
    btnPage1.className ="btn btn-warning active";
    btnPage2.className ="btn btn-warning ";
    btnPage3.className ="btn btn-warning ";
    btnPage4.className ="btn btn-warning ";
    btnPage5.className ="btn btn-warning ";
    console.log("page " + pageAdminSelector + " clicked");
});
btnPage2.addEventListener("click",function()
{
    pageAdminSelector = 2; 
    pageAdSelector();
    adminLabelSelectBoxes(39);
    btnPage1.className ="btn btn-warning ";
    btnPage2.className ="btn btn-warning active";
    btnPage3.className ="btn btn-warning ";
    btnPage4.className ="btn btn-warning ";
    btnPage5.className ="btn btn-warning ";
    console.log("page " + pageAdminSelector + " clicked");
});
btnPage3.addEventListener("click",function()
{
    pageAdminSelector = 3; 
    pageAdSelector();
    adminLabelSelectBoxes(79);
    btnPage1.className ="btn btn-warning ";
    btnPage2.className ="btn btn-warning ";
    btnPage3.className ="btn btn-warning active";
    btnPage4.className ="btn btn-warning ";
    btnPage5.className ="btn btn-warning ";
    console.log("page " + pageAdminSelector + " clicked");
});
btnPage4.addEventListener("click",function()
{
    pageAdminSelector = 4; 
    pageAdSelector();
    adminLabelSelectBoxes(119);
    btnPage1.className ="btn btn-warning ";
    btnPage2.className ="btn btn-warning ";
    btnPage3.className ="btn btn-warning ";
    btnPage4.className ="btn btn-warning active";
    btnPage5.className ="btn btn-warning ";
    console.log("page " + pageAdminSelector + " clicked");
});
btnPage5.addEventListener("click",function()
{
    pageAdminSelector = 5; 
    pageAdSelector();
    adminLabelSelectBoxes(159);
    btnPage1.className ="btn btn-warning ";
    btnPage2.className ="btn btn-warning ";
    btnPage3.className ="btn btn-warning ";
    btnPage4.className ="btn btn-warning ";
    btnPage5.className ="btn btn-warning active";
    console.log("page " + pageAdminSelector + " clicked");
});

//page selector
function pageAdSelector()
{
    switch(pageAdminSelector)
    {
        case 1:
                counter = page1Boundery[0];
                counterLength = page1Boundery[1];
                console.log("0-40");
                break;
        case 2:
                counter = page2Boundery[0];
                counterLength = page2Boundery[1];
                console.log("40-80");
                break;
        case 3:
                counter = page3Boundery[0];
                counterLength = page3Boundery[1];
                console.log("80-120");
                break;
        case 4:
                counter = page4Boundery[0];
                counterLength = page4Boundery[1];
                console.log("120-160");
                break;
        case 5:
                counter = page5Boundery[0];
                counterLength = page5Boundery[1];
                console.log("160-200");
                break;
    }

}

pageAdSelector();

//default labelstorage
if (readLabelsStorage() === null)
{
    WriteToLabelStorage(defaultObj);
    
    //storeLabelsAdminfirstTime();
    
   
    
}


//onload set labels from local storage
setLabels(counter);
adminLabelSelectBoxes(counter);

//fil table with selectboxes
function adminLabelSelectBoxes(index)
{
    let lib = readLibrary();
    let labelStorage =  readLabelsStorage();
    if(lib)
    {
            for( let i=index ; i < index + 40 ; i++)
            {
                for (let j=0 ; j< lib.length ;j++)
                { 
                    if(labelStorage[i].title === lib[j].title)
                    {
                        markup += `<option  id="${j}" name="${i}" value='${JSON.stringify(lib[j])}' selected="selected">${lib[j].title} - ${lib[j].artist}</option>`
                    }
                    else
                    {
                        markup += `<option  id="${j}" name="${i}" value='${JSON.stringify(lib[j])}'>${lib[j].title} - ${lib[j].artist}</option>`
                    }
                    
                }   

                select[i].insertAdjacentHTML('beforeend', markup);
                markup2 = markup;
                markup = "";    
            }  
            selectEdit.insertAdjacentHTML('beforeend', markup2);
            selectDelete.insertAdjacentHTML('beforeend', markup2);
            markup2 = ""; 
            
    }
}
        
//button action selectboxes
btnSetLabels.addEventListener("click", storeLabelsAdmin);

function storeLabelsAdmin()
{
    labels =  readLabelsStorage();
    //store in local storage
    for( i = counter ; i < counterLength ; i++)
    {      
        let selectedOption = JSON.parse(select[i].options[select[i].selectedIndex].value);
        //console.log(selectedOption);

                    if(pageAdminSelector === 1) {selectedOption.adress =  select[i].dataset.adress1;} 
                    else if(pageAdminSelector === 2) {selectedOption.adress =  select[i].dataset.adress2;}
                    else if(pageAdminSelector === 3) {selectedOption.adress =  select[i].dataset.adress3;}
                    else if(pageAdminSelector === 4) {selectedOption.adress =  select[i].dataset.adress4;}
                    else if(pageAdminSelector === 5) {selectedOption.adress =  select[i].dataset.adress5;}
                    
                    
                    labels.splice(i,1,selectedOption); //https://www.javascripttutorial.net/javascript-array-splice/
   
       
    }

    WriteToLabelStorage(labels);
    setLabels(counter);
    //reload
    location.reload();

}

function storeLabelsAdminfirstTime()
{
    labels =  readLabelsStorage();
    //store in local storage
    for( i = counter ; i < counterLength ; i++)
    {      
        let selectedOption = JSON.parse(select[i].options[select[i].selectedIndex].value);
        //console.log(selectedOption);

                    if(pageAdminSelector === 1) {selectedOption.adress =  select[i].dataset.adress1;} 
                    else if(pageAdminSelector === 2) {selectedOption.adress =  select[i].dataset.adress2;}
                    else if(pageAdminSelector === 3) {selectedOption.adress =  select[i].dataset.adress3;}
                    else if(pageAdminSelector === 4) {selectedOption.adress =  select[i].dataset.adress4;}
                    else if(pageAdminSelector === 5) {selectedOption.adress =  select[i].dataset.adress5;}
                    
                    
                    labels.splice(i,1,selectedOption); //https://www.javascripttutorial.net/javascript-array-splice/
   
       
    }

    WriteToLabelStorage(labels);
    setLabels(counter);
    //reload
    location.reload();

}

     
//functions
//------------------------

function setLabels(pageCount)
{
    //set labels and artist
    let j = 0 ; 
    
    let labelStorage =  readLabelsStorage();
    
    for( let i = 0 ; i < 40 ; i++)
    {
        if(pageCount < 39)
        {
            adr[i].innerText = labelStorage[i + pageCount].adress;
            lab[i].innerText = labelStorage[i + pageCount].title; 

            if(i%2==0)
            {
                labelArtist[j].innerText = labelStorage[i + pageCount].artist;
                j++;
            }
        }
        else
        {
            adr[i].innerText = labelStorage[i + pageCount + 1].adress;
            lab[i].innerText = labelStorage[i + pageCount + 1].title; 

            if(i%2==0)
            {
                labelArtist[j].innerText = labelStorage[i + pageCount +1].artist;
                j++;
            }
        }
        

    }
}













//opzoekingen

//var text= selectElement.options[selectElement.selectedIndex].text; //get the selected option text
