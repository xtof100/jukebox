//-----------------------------------------------------------------------------
//All functions that controls the library
//-----------------------------------------------------------------------------

//load songs in to library
btnAdd.addEventListener("click", loadSongDataInLibrary)

function loadSongDataInLibrary()
{
    if(fileNameAdd.value != "")
    {
        fileNameAdd.style.backgroundColor = "white";
          
        if(titleAdd.value != "")
        {
            titleAdd.style.backgroundColor = "white";
            if(artistAdd.value !="")
            {
                //set background to white (if it priviously triggered the red)
                artistAdd.style.backgroundColor = "white";
                
                //add data to song opbject
                song.file = fileNameAdd.value;
                song.title= titleAdd.value;
                song.artist= artistAdd.value;

                //add to Library array
                console.log(library);
             
                library.push(song);

                //write to file
                            
                
                WriteToLibrary(library);
                
                //give warning that data is added to the library
                alert("Data added to Library");

                //reset the inputs for a next song
                fileNameAdd.value ="";
                titleAdd.value = "";
                artistAdd.value = "";

                //debug
                console.log("songData" + song.file + "," + song.title + "," + song.artist);
                //console.log(lib);

                //reload
                location.reload();
            }
            else
            {
                artistAdd.style.backgroundColor = "red";
            }
        }
        else
        {
            titleAdd.style.backgroundColor = "red";
        }
    }

    else
    {
        fileNameAdd.style.backgroundColor = "red";
    }
    

}

//edit box code
//code for selectbox
document.getElementById('selectEdit').addEventListener('change', loadEditData );

function loadEditData()
{
    let editSelection = JSON.parse(selectEdit.options[selectEdit.selectedIndex].value);
    let tempLib= readLibrary();
    for (let i=0; i< tempLib.length ;i++)
    {
        if(tempLib[i].title === editSelection.title )
        {
            fileNameEdit.value = tempLib[i].file;
            titleEdit.value = tempLib[i].title ;
            artistEdit.value = tempLib[i].artist;
        }
    }
}
//store modified code in the library
btnEdit.addEventListener("click", storeEditData);

function storeEditData()
{
    let editSelection = JSON.parse(selectEdit.options[selectEdit.selectedIndex].value);
    let tempLib= readLibrary();
    for (let i=0; i< tempLib.length ;i++)
    {
        if(tempLib[i].title === editSelection.title )
        {
            tempLib[i].file = fileNameEdit.value;
            tempLib[i].title = titleEdit.value;
            tempLib[i].artist = artistEdit.value;
        }
    }

    localStorage.clear;
    localStorage.setItem('lib',JSON.stringify(tempLib));  

    //give warning that data is added to the library
    alert("Data "+ editSelection.title +" edited in Library");

    //reload
    location.reload();
}

//delete box code
//code for selectbox
btnDelete.addEventListener('click', deleteEntry );

function deleteEntry()
{
    let editSelection = JSON.parse(selectDelete.options[selectDelete.selectedIndex].value);
    let tempLib= readLibrary();
    for (let i=0; i< tempLib.length ;i++)
    {
        if(tempLib[i].title === editSelection.title )
        {
            tempLib.splice(i,1);
        }
    }

    localStorage.clear;
    localStorage.setItem('lib',JSON.stringify(tempLib));  
    //give warning that data is added to the library
    alert("Data "+ editSelection.title  +" deleted from Library");

    //reload
    location.reload();

}



