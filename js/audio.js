//audio control
//--------------------

//fill playList
    
function loadFirstPlayList()
{
   let trackStorage = readLabelsStorage();
   let playList = readPlayListNrsFromStorage();

   if(playList !== null)
   {
        for (let i=0 ; i< playList.length ; i++) 
        {
            for(let j=0; j < trackStorage.length ; j ++)
            {
                //link  adress to file name
                if(playList[i] == trackStorage[j].adress)  
                {
                    trackList.push(trackStorage[j]);
                }
            }
        } 
    }
    else
    {
        writeFirstEntry(1);
    }             
   console.log(trackList);
}

function addToPlayList()
{
   console.log("add to playlist activated");
   let trackStorage = readLabelsStorage();
   let playList = readPlayListNrsFromStorage();

   if(playList !== null)
   {
            for(let j = 0;  j < trackStorage.length ; j++)
            {
                //link  adress to file name
                if(playList[playList.length-1] == trackStorage[j].adress)  
                {
                    trackList.push(trackStorage[j]); 
                    
                }
            }
    }
    else
    {
        writeFirstEntry(1);
    }             
    console.log(trackList);
    songCount.innerText = trackList.length - trackIndex;
}


function playAudio()
{
    // Check if the player is selected
    if (music_player === null) 
    {
        throw "Playlist Player does not exists ...";
    } 
    else 
    {
        if(trackList.length> 0)
        {   
            // Start the player
            music_player.src = "/music/"+ trackList[trackIndex].file;
            //set info on infoscreen
            infoTitle.innerText = trackList[trackIndex].title;
            infoArtist.innerText = trackList[trackIndex].artist;
            songCount.innerText = trackList.length - trackIndex;

            //remove track from playlist
            

            // Listen for the music ended event, to play the next audio file
            music_player.addEventListener('ended', next, false)
        }
    }

}

// function for moving to next audio file
function next() 
{
    // Check for last audio file in the playlist
    if (trackIndex === trackList.length-1) 
    {
        trackIndex = 0;
    } 
    else 
    {
        trackIndex++;
    }

    //subtract one of songCount and set info screens
    songCount.innerText = trackList.length - trackIndex;
    infoTitle.innerText = trackList[trackIndex].title;
    infoArtist.innerText = trackList[trackIndex].artist;
    // Change the audio element source
    music_player.src = "/music/"+ trackList[trackIndex].file;
}

function removeTrackFromPlaylist()
{
    //remove first song from playlist   
    playListNrs = readPlayListNrsFromStorage(); //read from local storage
    playlistNrs.shift(); //removes first entry

    //localStorage.removeItem(playListNrs); //remove old (key of localstorage has same name then the variable i created)
    localStorage.removeItem("playListNrs");
    localStorage.setItem('playListNrs',JSON.stringify(playlistNrs));  //write new  
}


//---------------------------------------------------------------------------------------
//main

function hideShow()
{
    let tempReadHide= readHide();
    if(tempReadHide === "1")
    {
        admin.style.display = "none";
    }
    else if (tempReadHide === "0")
    {
        admin.style.display = "block";
    }
}

hideShow();

btnSettings.addEventListener("click", showHideSettings);

function showHideSettings() 
{
    if(readHide())
    {
        let tempHide = readHide();

        if(tempHide === "0" )
        {
            admin.style.display = "none";
            WriteHide(1);
        }
        else if(tempHide === "1")
        {
            admin.style.display = "block";
            WriteHide(0);
        }
    }
    else
    {
        WriteHide(1); //check if entry exists in local storage
    }

}




/* 
function showHideSettings() 
{    
    if (admin.style.display === "none") 
    {
      
    } 
    else 
    {
      
    }
} */







































/*
//---------------------------------------------------------------
//working
//audio
let tempAudio1 ="Estranged.mp3";
let tempAudio2 ="Rats.mp3";
playAudio(tempAudio1);
playAudio(tempAudio2);


function playAudio(trackToPlay)
{
    let audio = new Audio("/music/" + trackToPlay);
    audio.play() ;
}
*/